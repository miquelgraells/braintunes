package com.example.braintunes;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 *
 *Classe que permite customisar un list view mostrando el nombre de las canciones y sus imagenes
 *
 * @author Miquel Graells Monreal
 *
 */

public class MusicAdapter extends BaseAdapter implements View.OnClickListener {
    ArrayList<String> songsTitles;
    ArrayList<Bitmap> songsImages;
    LayoutInflater layoutInflater;
    ArrayList<MusicAdapter.MusicAdapterInterface> addMusicAdapterInterface = new ArrayList<>();
    /**
     *
     * @param context contexto para inflar el layout
     * @param song arrayList con los titulos de las canciones
     * @param songsImages arrayList con las imagenes de las canciones
     */
    public MusicAdapter(Context context, ArrayList<String> song, ArrayList<Bitmap> songsImages) {
        this.songsTitles = song;
        this.songsImages = songsImages;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return songsTitles.size();
    }

    @Override
    public Object getItem(int position) {
        return songsTitles.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public void onClick(View v) {
        for (int i = 0; i < addMusicAdapterInterface .size(); i++) {
            int position = (Integer) v.getTag();
            addMusicAdapterInterface .get(i).onSelectSong(position);
        }

    }

    // Si utilitzem el patró ViewHolder, el primer que farem és crear el model,
    // classe, per als objectes
    // que representen
    static class ViewHolder {
        TextView nameSong;
        ImageView imageSong;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            ViewHolder holder;

            if (convertView == null) {
                // Inflem el Layout de cada item
                convertView = layoutInflater.inflate(R.layout.item, null);
                holder = new ViewHolder();
                // Capturem els TextView
                holder.nameSong = (TextView) convertView
                        .findViewById(R.id.strangeWord);
                holder.imageSong = (ImageView) convertView
                        .findViewById(R.id.imageArtist);
                // Associem el viewholder,
                // la informació de l'estructura que hi ha d'haver dins el layout,
                // amb la vista que haurà de retornar aquest mètode.
                convertView.setTag(holder);

                holder.nameSong.setText("" + songsTitles.get(position));
                //holder.imageSong.setImageBitmap(songsImages.get(position));

            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            View songTitile = convertView.findViewById(R.id.strangeWord);

            songTitile.setTag(position);
            songTitile.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    public void addInterface(MusicAdapterInterface iface) {
        addMusicAdapterInterface.add(iface);
    }

    public interface MusicAdapterInterface {
        void onSelectSong(int index);
    }

}
