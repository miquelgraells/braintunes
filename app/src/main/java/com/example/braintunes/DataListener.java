package com.example.braintunes;

import com.choosemuse.libmuse.Muse;
import com.choosemuse.libmuse.MuseArtifactPacket;
import com.choosemuse.libmuse.MuseConnectionPacket;
import com.choosemuse.libmuse.MuseDataListener;
import com.choosemuse.libmuse.MuseDataPacket;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

class DataListener extends MuseDataListener {

    List<DataListenerInterface> dataListeners = new ArrayList<>();

    @Override
    public void receiveMuseDataPacket(final MuseDataPacket p, final Muse muse) {
        //activityRef.get().receiveMuseDataPacket(p, muse);
        for (DataListenerInterface dataListenerInterface : dataListeners) {
            dataListenerInterface.onReceiveMuseDataPacket(p, muse);
        }
    }

    @Override
    public void receiveMuseArtifactPacket(final MuseArtifactPacket p, final Muse muse) {
        //activityRef.get().receiveMuseArtifactPacket(p, muse);
        for (DataListenerInterface dataListenerInterface : dataListeners) {
            dataListenerInterface.onReceiveMuseArtifactPacket(p, muse);
        }
    }

    public void addListener(DataListenerInterface iface) {
        dataListeners.add(iface);
    }

    public interface DataListenerInterface {
        void onReceiveMuseDataPacket(final MuseDataPacket p, final Muse muse);
        void onReceiveMuseArtifactPacket(final MuseArtifactPacket p, final Muse muse);
    }

}
