package com.example.braintunes;

import com.choosemuse.libmuse.Muse;
import com.choosemuse.libmuse.MuseConnectionListener;
import com.choosemuse.libmuse.MuseConnectionPacket;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

class ConnectionListener extends MuseConnectionListener {

    List<ConnectionListenerInterface> connectionListeners = new ArrayList<>();

    @Override
    public void receiveMuseConnectionPacket(final MuseConnectionPacket p, final Muse muse) {
        //activityRef.get().receiveMuseConnectionPacket(p, muse);
        for (ConnectionListenerInterface connectionListener : connectionListeners) {
            connectionListener.onReceiveMuseConnectionPacket(p, muse);
        }
    }

    public void addListener(ConnectionListenerInterface iface) {
        connectionListeners.add(iface);
    }

    public interface ConnectionListenerInterface {
        void onReceiveMuseConnectionPacket(final MuseConnectionPacket p, final Muse muse);
    }
}
