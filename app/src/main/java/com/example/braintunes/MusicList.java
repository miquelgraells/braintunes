package com.example.braintunes;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MusicList.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MusicList#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MusicList extends Fragment implements LoadSongs.OnResponse, MusicAdapter.MusicAdapterInterface {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    View v = null;
    ListView musiclist = null;

    ArrayList<String> titlesList = new ArrayList<String>(); // array con los titulos de las canciones
    ArrayList<Bitmap> musicImageList = new ArrayList<Bitmap>(); // array con las imagenes de las canciones
    ArrayList<String> dataMusic = new ArrayList<String>(); // array con los path de las canciones

    public MusicList() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MusicList.
     */
    // TODO: Rename and change types and number of parameters
    public static MusicList newInstance(String param1, String param2) {
        MusicList fragment = new MusicList();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        LoadSongs ls = new LoadSongs(getActivity(), 1);
        ls.addOnOnResponse(this);
        //ls.doInBackground(null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_music_list, container, false);

        //ArrayList<String> titles = getSongsTitles();
        getSongsTitles();
        MusicAdapter musicAdapter = new MusicAdapter(getContext(), titlesList,
                musicImageList);
        musicAdapter.addInterface(this);
        /*MusicAdapter musicAdapter = new MusicAdapter(getContext(), titles,
                musicImageList);*/
        // añadimos el adaptador a la listview
        musiclist = (ListView) v.findViewById(R.id.musicList);
        musiclist.setAdapter(musicAdapter);

        // loading the songs here

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onLoadSongs(ArrayList<String> titles, ArrayList<Bitmap> images, ArrayList<String> paths) {
        titlesList.addAll(titles);
        musicImageList.addAll(images);

        if (v != null) {
            MusicAdapter musicAdapter = new MusicAdapter(getContext(), titlesList,
                    musicImageList);
            // añadimos el adaptador a la listview
            musiclist = (ListView) v.findViewById(R.id.musicList);
            musiclist.setAdapter(musicAdapter);
        }

    }

    private void getSongsTitles () {


        //ArrayList<String> titles = new ArrayList<String>(); // array con
        try {
            Bitmap bm = null;
            Bitmap icon = null;
            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            ContentResolver contentResolver = getActivity().getContentResolver();
            //Uri uri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            //Uri uri = android.provider.MediaStore.Audio.Media.INTERNAL_CONTENT_URI;
            //Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";
            // cursor del cual podremos obtener el path de la cancion, el titulo
            // , y las imagenes
            Cursor musiccursor = contentResolver.query(uri, null, selection, null, null);
            // definimos el tamaño de los array sabiendo cuantas canciones
            // tenemos
            if (musiccursor == null) {
                // query failed, handle error.
            } else if (!musiccursor.moveToFirst()) {
                // no media on the device
//			Toast.makeText(this, "No sdcard presents", Toast.LENGTH_SHORT)
//					.show();
                Log.d("songs", "no hay sdcard");
            } else {
                // obtenemos el indice de la columna en la que se guarda el
                // titulo de la cancion dentro del cursor
                int titleColumn = musiccursor
                        .getColumnIndex(android.provider.MediaStore.Audio.Media.DISPLAY_NAME);
                // obtenemos el indice de la columna en la que se guarda el path
                // de la cancion dentro del cursor
                int data = musiccursor.getColumnIndex(MediaStore.Audio.Media.DATA);

                // recoremos de todas las filas del cursor
                for (int i = 0; i < musiccursor.getCount(); i++) {
                    // obtenoms el titulo de la cancion de la columna de titulos
                    // del cursor y lo añadimos al array correspondiente
                    titlesList.add(musiccursor.getString(titleColumn));
                    // obtenoms el path de la cancion de la columna de paths del
                    // cursor y lo añadimos al array correspondiente
                    dataMusic.add(musiccursor.getString(data));
                    /*dataMusic.add(musiccursor.getString(data));

                    // le pasamos al objeto que nos permitira obtener las
                    // imagenes el path de la cancions
                    mmr.setDataSource(musiccursor.getString(data));
                    // obtenemos la imagen
                    byte[] artBytes = mmr.getEmbeddedPicture();
                    // si tiene imagen la añadimos al array corespondiente
                    if (artBytes != null) {
                        //pasamos la imagen de bytes a bitmap

                        //bm = BitmapFactory.decodeByteArray(artBytes, 0,
                        //	artBytes.length);



                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inSampleSize = 8;
                        bm = BitmapFactory.decodeByteArray(artBytes, 0,
                                artBytes.length,options);



                        musicImageList.add(bm);
                    }else{
                        //por el contrario añadimos una imagen predeterminada
                        musicImageList.add(icon);
                    }*/
                    // nos movemos a la siguiente fila del cursor
                    musiccursor.moveToNext();
                }

                Log.d("myApp", "grid");
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("No sdcard presents");
            Log.d("myApp", "grid error");
        }
        //return titles;

    }

    @Override
    public void onSelectSong(int index) {
        Context context = getContext();
        CharSequence text = "song number " + String.valueOf(index);
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();

        String songpath = dataMusic.get(index);
        Uri myUri = Uri.parse(songpath);

        mListener.onFragmentInteraction(myUri);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
