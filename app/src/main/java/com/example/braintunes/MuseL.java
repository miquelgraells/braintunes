package com.example.braintunes;

import com.choosemuse.libmuse.MuseListener;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

class MuseL extends MuseListener {

    List<MuseLInterface> museLlisteners = new ArrayList<>();


    @Override
    public void museListChanged() {
        for (MuseLInterface museLlistener : museLlisteners) {
            museLlistener.onListChanged();
        }
    }

    public void addListener(MuseLInterface iface) {
        museLlisteners.add(iface);
    }

    public interface MuseLInterface {
        void onListChanged();
    }
}

