package com.example.braintunes;

import android.content.Context;
import android.media.audiofx.Visualizer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import com.choosemuse.libmuse.Accelerometer;
import com.choosemuse.libmuse.Eeg;
import com.choosemuse.libmuse.Muse;
import com.choosemuse.libmuse.MuseArtifactPacket;
import com.choosemuse.libmuse.MuseDataPacket;
import com.choosemuse.libmuse.MuseDataPacketType;
import com.choosemuse.libmuse.MuseManagerAndroid;
import com.gauravk.audiovisualizer.visualizer.BarVisualizer;
import com.gauravk.audiovisualizer.visualizer.BlastVisualizer;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MusicPlayer.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MusicPlayer#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MusicPlayer extends Fragment implements DataListener.DataListenerInterface {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private final double[] eegBuffer = new double[6];
    private boolean eegStale;
    private final double[] alphaBuffer = new double[6];
    private boolean alphaStale;
    private final double[] accelBuffer = new double[3];
    private boolean accelStale;

    static String MP4_VIDEO_URI = "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4";

    private BarVisualizer mVisualizer;
    private AudioPlayer mAudioPlayer;

    private MuseManagerAndroid manager;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    //private DataListener dataListener;

    DataListener dataListener;

    LineChart chart;
    List<Entry> entries0, entries1, entries2, entries3;
    LineDataSet dataSet;

    long lastTimestamp = 0;

    int i = 0;
    int e = 0;

    private Muse muse;

    public MusicPlayer() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MusicPlayer.
     */
    // TODO: Rename and change types and number of parameters
    public static MusicPlayer newInstance(Uri uri) {
        MusicPlayer fragment = new MusicPlayer();
        MP4_VIDEO_URI = uri.toString();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_music_player, container, false);


        mVisualizer = v.findViewById(R.id.bar);
        mAudioPlayer = new AudioPlayer();

        chart = (LineChart) v.findViewById(R.id.chart);
        chart.getAxisLeft().setDrawGridLines(false);
        chart.getXAxis().setDrawGridLines(false);
        //chart.setstar.setStartAtZero(boolean enabled)

        entries0 = new ArrayList<Entry>();
        entries1 = new ArrayList<Entry>();
        entries2 = new ArrayList<Entry>();
        entries3 = new ArrayList<Entry>();

        manager = MuseManagerAndroid.getInstance();
        manager.setContext(getActivity());

        dataListener = new DataListener();
        dataListener.addListener(this);

        muse = manager.getMuses().get(0);

        muse.registerDataListener(dataListener, MuseDataPacketType.EEG);

        return v;
    }


    @Override
    public void onStart() {
        super.onStart();
        //startPlayingAudio(R.raw.sample);
        startPlayingAudio(Uri.parse(MP4_VIDEO_URI));
    }

    @Override
    public void onStop() {
        super.onStop();
        stopPlayingAudio();
    }

    private void stopPlayingAudio() {
        if (mAudioPlayer != null)
            mAudioPlayer.stop();
        if (mVisualizer != null)
            mVisualizer.release();
    }

    private void startPlayingAudio(Uri resId) {
        mAudioPlayer.play(getContext(), resId, new AudioPlayer.AudioPlayerEvent() {
            @Override
            public void onCompleted() {
                if (mVisualizer != null)
                    mVisualizer.hide();
            }
        });
        int audioSessionId = mAudioPlayer.getAudioSessionId();
        if (audioSessionId != -1)
            mVisualizer.setAudioSessionId(audioSessionId);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void getEegChannelValues(double[] buffer, MuseDataPacket p) {
        long timestamp = p.timestamp();
        buffer[0] = p.getEegChannelValue(Eeg.EEG1);
        buffer[1] = p.getEegChannelValue(Eeg.EEG2);
        buffer[2] = p.getEegChannelValue(Eeg.EEG3);
        buffer[3] = p.getEegChannelValue(Eeg.EEG4);
        buffer[4] = p.getEegChannelValue(Eeg.AUX_LEFT);
        buffer[5] = p.getEegChannelValue(Eeg.AUX_RIGHT);

        String eeg0 = String.valueOf(buffer[0]);
        String eeg1 = String.valueOf(buffer[1]);
        String eeg2 = String.valueOf(buffer[2]);
        String eeg3 = String.valueOf(buffer[3]);

        double updateTime = timestamp - lastTimestamp;

        if (updateTime > 150000) {
            lastTimestamp = timestamp;

            Float yEeg0 = Float.valueOf(eeg0);
            Float yEeg1 = Float.valueOf(eeg1);
            Float yEeg2 = Float.valueOf(eeg2);
            Float yEeg3 = Float.valueOf(eeg3);


            entries0.add(new Entry(i, yEeg0));
            LineDataSet dataSet0 = new LineDataSet(entries0, "EEG0");
            //dataSet0.setAxisDependency(YAxis.AxisDependency.LEFT);
            dataSet0.setDrawValues(false);
            dataSet0.setDrawCircles(false);
            dataSet0.setColor(R.color.eeg0);

            entries1.add(new Entry(i, yEeg1));
            LineDataSet dataSet1 = new LineDataSet(entries1, "EEG1");
            //dataSet1.setAxisDependency(YAxis.AxisDependency.LEFT);
            dataSet1.setDrawValues(false);
            dataSet1.setDrawCircles(false);
            dataSet1.setColor(R.color.eeg1);
            //dataSet.setLineWidth(200);

            entries2.add(new Entry(i, yEeg2));
            LineDataSet dataSet2 = new LineDataSet(entries2, "EEG2");
            //dataSet2.setAxisDependency(YAxis.AxisDependency.LEFT);
            dataSet2.setDrawValues(false);
            dataSet2.setDrawCircles(false);
            dataSet2.setColor(R.color.eeg2);

            entries3.add(new Entry(i, yEeg3));
            LineDataSet dataSet3 = new LineDataSet(entries3, "EEG3");
            //dataSet3.setAxisDependency(YAxis.AxisDependency.LEFT);
            dataSet3.setDrawValues(false);
            dataSet3.setDrawCircles(false);
            dataSet3.setColor(R.color.eeg3);

            if (entries0.size() > 11) {
                dataSet0.removeFirst();
            }
            if (entries1.size() > 11) {
                dataSet1.removeFirst();
            }
            if (entries2.size() > 11) {
                dataSet2.removeFirst();
            }
            if (entries3.size() > 11) {
                dataSet3.removeFirst();
            }

            i++;

            List<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
            dataSets.add(dataSet0);
            dataSets.add(dataSet1);
            dataSets.add(dataSet2);
            dataSets.add(dataSet3);

            LineData lineData = new LineData(dataSets);
            chart.setData(lineData);
            chart.notifyDataSetChanged();
            chart.invalidate();

        }
    }

    private void getAccelValues(MuseDataPacket p) {
        accelBuffer[0] = p.getAccelerometerValue(Accelerometer.X);
        accelBuffer[1] = p.getAccelerometerValue(Accelerometer.Y);
        accelBuffer[2] = p.getAccelerometerValue(Accelerometer.Z);
    }

    @Override
    public void onReceiveMuseDataPacket(final MuseDataPacket p, final Muse muse) {
        final long n = p.valuesSize();
        switch (p.packetType()) {
            case EEG:
                assert(eegBuffer.length >= n);
                getEegChannelValues(eegBuffer,p);
                eegStale = true;
                break;
            case ACCELEROMETER:
                assert(accelBuffer.length >= n);
                getAccelValues(p);
                accelStale = true;
                break;
            case ALPHA_RELATIVE:
                assert(alphaBuffer.length >= n);
                getEegChannelValues(alphaBuffer,p);
                alphaStale = true;
                break;
            case BATTERY:
            case DRL_REF:
            case QUANTIZATION:
            default:
                break;
        }
    }

    @Override
    public void onReceiveMuseArtifactPacket(MuseArtifactPacket p, Muse muse) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
